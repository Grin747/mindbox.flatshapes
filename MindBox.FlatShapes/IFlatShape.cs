﻿namespace MindBox.FlatShapes;

public interface IFlatShape
{
    public double GetPerimeter();
    public double GetArea();
}