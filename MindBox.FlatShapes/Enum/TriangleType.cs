namespace MindBox.FlatShapes.Enum;

public enum TriangleType
{
    Right,
    Obtuse,
    Acute
}