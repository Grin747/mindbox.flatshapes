# Тестовое задание Junior/Middle C#
## Вопрос №1
Разместите код на Github и приложите ссылку. Если в задании что-то непонятно, опишите возникшие вопросы и сделанные предположения. Например, в комментариях в коде.
Задание:
Напишите на C# библиотеку для поставки внешним клиентам, которая умеет вычислять площадь круга по радиусу и треугольника по трем сторонам. Дополнительно к работоспособности оценим:
Юнит-тесты
Легкость добавления других фигур
Вычисление площади фигуры без знания типа фигуры в compile-time
Проверку на то, является ли треугольник прямоугольным
## Вопрос №2
В базе данных MS SQL Server есть продукты и категории. Одному продукту может соответствовать много категорий, в одной категории может быть много продуктов. Напишите SQL запрос для выбора всех пар «Имя продукта – Имя категории». Если у продукта нет категорий, то его имя все равно должно выводиться.
По возможности — положите ответ рядом с кодом из первого вопроса.

~~~sql
create table products
(
    id int identity not null primary key,
    title varchar(255)
);

insert into products 
values ('Bread'), ('Cheese'), ('Berry bun'), ('Yogurt'), ('Pork'), ('Cinnabon'), ('Falafel');

create table categories
(
    id int identity not null primary key,
    title varchar(255)
);

insert into categories
values ('Meat'), ('Dairy'), ('Bakery'), ('Dessert');

create table productToCategory
(
    productId  int,
    categoryId int,

    constraint PK_ProductsInCategories primary key (productId, categoryId),
    constraint FK_ProductId foreign key (productId) references products (Id),
    constraint FK_CategoryId foreign key (categoryId) references categories (Id)
);

insert into productToCategory
values (1, 3), (2, 2), (3, 3), (3, 4), (4, 2), (4, 4), (5, 1), (6, 3), (6, 4);

select p.title, c.title
from products p
    left join productToCategory pc on p.id = pc.productId
    left join categories c on c.Id = pc.categoryId;